# The packaging information for python flask wtforms in XiVO

This repository contains the packaging information for
[python-flask-wtf](https://github.com/lepture/flask-wtf).

To get a new version of python-flask-wtf in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
